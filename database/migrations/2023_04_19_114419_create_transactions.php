<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->unsignedBigInteger('remitter_id')->nullable();
            $table->unsignedBigInteger('beneficiary_id')->nullable();
            $table->unsignedBigInteger('wallet_id')->nullable();
            $table->float('amount')->nullable();
            $table->dateTime('date_time')->nullable();
            $table->text('remark')->nullable();
            $table->string('status')->nullable();
            $table->enum('type', ['cash-plus', 'cash-minus'])->nullable();
            $table->foreign('wallet_id')->references('id')->on('wallets')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
