<?php

namespace App\Helpers;

class Helper
{
    public static function generateKey()
    {
        // Tạo public key và private key
        $config = [
            "digest_alg"       => "sha512",
            "private_key_bits" => 4096,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        ];
        $res    = openssl_pkey_new($config);
        openssl_pkey_export($res, $private_key);
        $public_key = openssl_pkey_get_details($res);
        $public_key = $public_key["key"];

        return $public_key;

        // Hàm mã hóa OTP
        function encryptOTP($otp, $public_key): string
        {
            $ciphertext = '';
            openssl_public_encrypt($otp, $ciphertext, $public_key);

            return base64_encode($ciphertext);
        }

        // Hàm giải mã OTP
        function decryptOTP($ciphertext, $private_key): string
        {
            $ciphertext = base64_decode($ciphertext);
            $decrypted  = '';
            openssl_private_decrypt($ciphertext, $decrypted, $private_key);

            return $decrypted;
        }

        // Sử dụng hàm mã hóa và giải mã
        $otp           = "123456"; // OTP cần mã hóa
        $encrypted_otp = encryptOTP($otp, $public_key); // Mã hóa OTP
        $decrypted_otp = decryptOTP($encrypted_otp, $private_key); // Giải mã OTP
    }
}
