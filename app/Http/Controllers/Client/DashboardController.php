<?php

namespace App\Http\Controllers\Client;

use App\Events\NotificationEvent;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Arrival;
use App\Models\Banner;
use App\Models\Blog;
use App\Models\Notification;
use App\Models\Product;
use App\Models\RequestOrder;
use App\Models\User;
use App\Models\VnpayTest;
use App\Models\Wallet;
use App\Models\wishlist;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Response;

class DashboardController extends Controller
{
    public function index()
    {
        $user_id     = Auth()->user()->id ?? null;
        $bannerSlide = Banner::query()->where('status', '=', 'active')->orderBy('created_at', 'DESC')->limit(5)->get();
        $arrivals    = Arrival::all();
        $products    = Product::all();
        $blogs       = Blog::all();
        // dd($blogs);
        // return view('client.index',compact('bannerSlide','arrivals', 'products','blogs'));
        // Mới nhất
        $lproducts = Product::orderBy('created_at', 'DESC')->get()->take(8);
        // nổi bật
        $fproducts = Product::where('status', 'active')->orderBy('updated_at', 'DESC')->get()->take(8);

        return view('client.index',
                    compact('bannerSlide', 'arrivals', 'fproducts', 'products', 'lproducts', 'bannerSlide', 'arrivals',
                            'products', 'blogs'));

    }

    public function myaccount()
    {

        $user = User::where('id', Auth()->user()->id)->first();

        return view('client.myaccount', compact('user'));
    }

    public function wallet()
    {

        $wallet = Wallet::where('user_id', Auth()->user()->id)->first();

        return view('client.wallet', compact('wallet'));
    }

    public function postWallet(Request $request)
    {
        // Tạo số tài khoản ngẫu nhiên
        $faker          = Faker::create();
        $account_number = $faker->numberBetween(1000000000, 9999999999);

        // Kiểm tra xem số tài khoản đã tồn tại hay chưa
        $wallet = Wallet::where('account_number', $account_number)->first();
        if ($wallet) {
            // Nếu số tài khoản đã tồn tại, tạo số tài khoản mới
            return $this->postWallet($request);
        }

        // Tạo bản ghi mới với số tài khoản ngẫu nhiên
        Wallet::create([
                           'account_number' => $account_number,
                           'user_id'        => Auth::id(),
                           'amount'         => 0,
                           'active'         => true
                       ]);

        return redirect()->back();
    }

    public function address()
    {
        $address = Address::where('user_id', Auth()->user()->id)->get();

        return view('client.address', compact('address'));
    }

    public function postMyaccount(Request $request, Response $response)
    {
        $infomation = User::where('id', Auth()->user()->id)
                          ->update([
                                       'name'     => $request->input('name'),
                                       'phone'    => $request->input('phone'),
                                       'birthday' => date("Y-m-d", strtotime(str_replace('/', '-',
                                                                                         $request->input('birthday')))),
                                       'gender'   => $request->input('gender'),
                                   ]);

        return redirect()->back();
        // ->setError('Lỗi')
        // ->setMessage('Thành công');
    }

    public function postAddress(Request $request)
    {
        $check = Address::where('user_id', Auth()->user()->id)->count();
        if ($check >= 3) {
            return redirect()->back()->with('đã lưu 3 địa chỉ');
        } else {
            $address = Address::create([
                                           'name'         => $request->input('name'),
                                           'phone'        => $request->input('phone'),
                                           'city'         => $request->input('city'),
                                           'district'     => $request->input('district'),
                                           'ward'         => $request->input('ward'),
                                           'detailadress' => $request->input('detaileadress'),
                                           'data'         => $request->input('detaileadress') . '--' . $request->input('ward') . '--' . $request->input('district') . '--' . $request->input('city'),
                                           'status'       => 0,
                                           'user_id'      => Auth()->user()->id,
                                       ]);

            return view('client.address', compact('address'));
        }
    }

    public function orders(Request $request)
    {
        $user = $request->user();
        $user->id;
        $orders = VnpayTest::where('user_id', '=', $user->id)->orderBy('updated_at', 'desc')->get();

        $finishedOrders = VnpayTest::query()->where('user_id', '=', $user->id)
                                   ->where('status_order', '=', 'success')->get();

        if ($finishedOrders) {
            foreach ($finishedOrders as $order) {
                if (Carbon::parse($order->updated_at)->addDay(3) <= Carbon::now()) {
                    $order->update([
                                       'status_order' => 'finished'
                                   ]);
                }
            }
        }

        return view('client.order', compact('orders'));
    }

    public function orderFinished($id)
    {
        $order = VnpayTest::find($id);
        if ($order->status_order == "success") {
            $order->update([
                               'status_order' => 'finished'
                           ]);
        }

        return redirect()->back();
    }

    public function detailorder($id)
    {
        $detailorder = VnpayTest::where('id', '=', $id)->first();
        $cart        = $detailorder->cart;
        $test        = json_decode($cart, true);

        // dd($detailorder);
        return view('client.detail-order', compact('detailorder', 'test'));
    }

    public function cancelOrder($id)
    {
        $vnpay = VnpayTest::find($id);
        $vnpay->fill([
                         'status_order' => 'cancel'
                     ]);
        $vnpay->save();

        return redirect()->back()->with('msg', 'Hủy đơn hàng thành công!');
    }

    public function errorOrderForm($id)
    {
        $order   = VnpayTest::find($id);
        $cart    = $order->cart;
        $product = json_decode($cart, true);

        return view('client.error', compact('order', 'product'));
    }

    public function errorOrderSave($id, Request $request)
    {
        $user = $request->user();
        $user->id;
        $request_order = new RequestOrder();
        $request_order->fill([
                                 'id_user'      => $user->id,
                                 'id_order'     => $id,
                                 'name_product' => $request->name_product,
                                 'note'         => $request->note,
                                 'type'         => 'C',
                             ]);
        if ($request->hasFile('image')) {
            $newFileName          = uniqid() . '-' . $request->image->getClientOriginalName();
            $path                 = $request->image->storeAs('public/uploads/products', $newFileName);
            $request_order->image = str_replace('public/', '', $path);
        }
        $request_order->save();
        event(new NotificationEvent('Bạn vừa có đơn hàng báo lỗi'));

        return redirect()->route('order.detail', ['id' => $id])->with('msg', 'Báo lỗi thành công');
    }

    public function wishlist()
    {
        $wishlists    = Wishlist::where('user_id', '=', Auth::user()->id)->get();
        $wishlists_id = [];
        foreach ($wishlists as $key => $value) {
            array_push($wishlists_id, $value->product_id);
        }
        $products = Product::whereIn('id', $wishlists_id)->get();

        return view('client.wishlist', compact('wishlists', 'products'));
    }

    public function postWishlist($id)
    {
        $product   = Product::where('id', $id)->first();
        $wishlists = Wishlist::create([
                                          'product_id' => $product->id,
                                          'user_id'    => Auth::user()->id,
                                          'price'      => $product->price,
                                      ]);

        return redirect()->back();
    }

    public function updateNotification()
    {
        $allNotifications = Notification::where('user_id', Auth::user()->id)->update(['read_at' => 1]);

        return $allNotifications;
    }

    public function getEncrypt($id)
    {
        $data = VnpayTest::query()->where('id', '=', $id)->first();

        $folderPath = base_path() . '/RSA/store_public_key.txt'; // Đường dẫn đến thư mục cần đọc
        $publicKey  = file_get_contents($folderPath);

        // if (!$data->encrypt) {
        //     $data->encrypt = $this->encryptOTP($data->vnp_TxnRef, $publicKey);
        //
        //     $data->save();
        // }
        $data->encrypt = $this->encryptOTP($data->vnp_TxnRef, $publicKey);

        return $data->encrypt;
    }

    public function orderConfirm($id, Request $request)
    {
        if (!$request->message) {
            toastr()->warning('Hãy nhập thông điệp trước khi gửi', 'Lỗi');

            return redirect()->back()->with('message', 'Hãy nhập thông điệp trước khi gửi');
        }

        if (!$request->encryptCode) {
            toastr()->warning('Hãy nhập chữ ký trước khi gửi', 'Lỗi');

            return redirect()->back()->with('message', 'Hãy nhập chữ ký trước khi gửi');
        }
        $encryptById = VnpayTest::query()->where('id', '=', $request->verify)->first();
        if ($encryptById->status_order === 'draft') {
            $response = Http::post(env('VERIFY_URL'), [
                "message"    => $request->message,
                "blind_sign" => $request->encryptCode,
            ])->json();

            if ($response == 1) {
                $folderPath = base_path() . '/RSA/store_private_key.txt'; // Đường dẫn đến thư mục cần đọc
                $privateKey = file_get_contents($folderPath);
                $vnpTxnRef  = $this->decrypt($request->message, $privateKey);
                if ($vnpTxnRef == $encryptById->vnp_TxnRef) {
                    $encryptById->update([
                                             'status_order' => 'pending',
                                             'encrypt'      => '',
                                         ]);
                    toastr()->success('Xác nhận đơn hàng thành công', 'Thành công');

                    return redirect()->route('orders')->with('messageSuccess', 'Xác nhận đơn hàng thành công');
                } else {
                    toastr()->error('Mã đơn hàng không lệ', 'Lỗi');

                    return redirect()->back()->with('message', 'Mã đơn hàng không lệ');
                }

            } else {
                toastr()->error('Chữ ký không hợp lệ, hãy kiểm tra lại', 'Lỗi');

                return redirect()->back()->with('message', 'Chữ ký không hợp lệ, hãy kiểm tra lại');
            }
        } else {
            toastr()->error('Chữ ký của bản ghi không tồn tại hoặc trạng thái đơn hàng không hợp lệ', 'Lỗi');

            return redirect()->back()->with('message',
                                            'Chữ ký của bản ghi không tồn tại hoặc trạng thái đơn hàng không hợp lệ');
        }
    }

    public function encryptOTP($otp, $publicKey): string
    {
        $ciphertext = '';
        openssl_public_encrypt($otp, $ciphertext, $publicKey);

        return base64_encode($ciphertext);
    }

    public function decrypt($ciphertext, $privateKey): string
    {
        $privateKey = openssl_pkey_get_private($privateKey);
        if (!$privateKey) {
            // nếu khóa riêng tư không hợp lệ, trả về FAIL
            return false;
        }
        $ciphertext = base64_decode($ciphertext);
        $decrypted  = '';
        // giải mã
        if (!openssl_private_decrypt($ciphertext, $decrypted, $privateKey)) {
            // xử lý lỗi ở đây nếu cần thiết
            return false;
        }

        return $decrypted;
    }
}
