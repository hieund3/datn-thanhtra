<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Key;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class RSAController extends Controller
{
    public Model|Builder $model;

    public function __construct()
    {
        $this->model = new Key();
    }

    public function create(Request $request): BinaryFileResponse
    {
        // Tạo public key và private key
        $config = [
            "digest_alg"       => "sha512",
            "private_key_bits" => 4096,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        ];

        $res = openssl_pkey_new($config);
        openssl_pkey_export($res, $privateKey);
        $publicKey = openssl_pkey_get_details($res);
        $publicKey = $publicKey["key"];

        // Lưu public key vào DB
        $user            = Auth::user();
        $key             = $this->model->where('user_id', $user->id)->first() ?? new Key();
        $key->user_id    = $user->id;
        $key->public_key = $publicKey;
        $key->save();

        //Download
        $filename = convert_name($user->name) . '_private_key.txt';
        header("Content-Type: text/plain");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Length: " . strlen($privateKey));
        // Trả về nội dung của tệp tin
        echo $privateKey;
        exit;
    }

    // Hàm mã hóa OTP
    public function encryptOTP($otp, $publicKey): string
    {
        $ciphertext = '';
        openssl_public_encrypt($otp, $ciphertext, $publicKey);

        return base64_encode($ciphertext);
    }

    // Hàm giải mã OTP
    public function decryptOTP($ciphertext, $privateKey): string
    {
        $ciphertext = base64_decode($ciphertext);
        $decrypted  = '';
        openssl_private_decrypt($ciphertext, $decrypted, $privateKey);

        return $decrypted;
    }

}
