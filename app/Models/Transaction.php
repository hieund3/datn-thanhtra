<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use  HasFactory;

    protected $fillable
        = [
            'code',
            'remitter_id',
            'beneficiary_id',
            'wallet_id',
            'amount',
            'date_time',
            'remark',
            'status',
            'type',
            'created_at',
            'updated_at',
        ];

    public function remitter(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function beneficiary(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function wallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class);
    }
}
