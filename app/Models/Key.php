<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Key extends Model
{
    use  HasFactory;

    protected $fillable
        = [
            'user_id',
            'public_key',
            'created_by',
            'created_at',
            'updated_at',
        ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
