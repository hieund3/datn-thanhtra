<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Wallet extends Model
{
    use HasFactory;

    public    $guarded = [];
    protected $table   = 'wallets';
    protected $fillable
                       = [
            'account_number',
            'pin_code',
            'user_id',
            'amount',
            'active',
            'created_by',
            'created_at',
            'updated_at',
        ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
