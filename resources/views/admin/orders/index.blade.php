@extends('layouts.admin-layout')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2>Danh sách đơn hàng</h2>
                    @if(Session::has('msg'))
                        <div class="alert alert-success" role="alert">{{Session::get('msg')}}</div>
                    @endif

                    <div class="row">
                        <div class="col">
                            <form action="{{ route('admin.order.list') }}" method="get">
                                @csrf
                                <div class="row mt-2">
                                    <div class="col">
                                        Tìm kiếm theo trạng thái
                                    </div>
                                    <div class="col">
                                        <select class="select" style="padding: 2px; width: 80%;"
                                                name="searchStatus">
                                            <option value="all">Tất cả</option>
                                            <option value="pending">Chờ xử lý</option>
                                            <option value="shipping">Đang giao hàng</option>
                                            <option value="success">Đã giao hàng</option>
                                            <option value="cancel">Đã hủy</option>
                                            <option value="finished">Đã hoàn thành</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col">
                                        Tìm kiếm theo mã đơn hàng
                                    </div>
                                    <div class="col">
                                        <input type="text" name="searchId" style="padding: 2px; width: 80%;">
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <button type="submit" class="btn btn-primary" style="padding: 4px;">Tìm kiếm
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="col">
                            <form action="{{ route('product.export') }}">
                                @csrf
                                <div class="row mt-2">
                                    <div class="col-3">
                                        Tháng
                                    </div>
                                    <div class="col">
                                        <input type="number" name="month" id="month" min="1" max="12" required
                                               style="padding: 2px; width: 30%;">
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-3">
                                        <label for="">Năm</label>
                                    </div>
                                    <div class="col">
                                        <input type="number" name="year" pattern="\d{4}" min="2000" id="year" required
                                               style="padding: 2px; width: 30%;">
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <button class="btn btn-success" style="padding: 4px;">
                                        Xuất file CSV
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">STT</th>
                                <th scope="col">Đơn hàng</th>
                                <th scope="col">Người mua</th>
                                <th scope="col">Thanh toán</th>
                                <th scope="col">Chi tiết đơn hàng</th>
                                <th scope="col">Tình trạng đơn hàng</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$order->vnp_TxnRef}}</td>
                                    <td>{{$order->vnp_Bill_FirstName . ' ' . $order->vnp_Bill_LastName}}</td>
                                    @if($order->vnp_TransactionStatus == '00')
                                        <td>Đã thanh toán thành công</td>
                                    @elseif($order->vnp_TransactionStatus == null)
                                        <td>Thanh toán thất bại</td>
                                    @else
                                        <td></td>
                                    @endif

                                    <!-- Chi tiết đơn hàng -->
                                    <td><a href="{{route('admin.order.detail',['id'=>$order->id])}}">Xem</a></td>

                                    <!-- Trạng thái đơn hàng -->
                                    @if($order->status_order == 'draft' && $order->vnp_TransactionStatus == '00')
                                        <td class="btn btn-warning"
                                            style="padding: 2px; margin-top: 12px; width: 60%; border: 1px solid #FF9500">
                                            Chờ xác nhận
                                        </td>
                                    @elseif($order->status_order == 'pending' && $order->vnp_TransactionStatus == '00')
                                        <td class="btn btn-warning"
                                            style="padding: 2px; margin-top: 12px; width: 60%; border: 1px solid #FF9500">
                                            Đang xử
                                            lý
                                        </td>
                                    @elseif($order->status_order == 'outStock')
                                        <td class="btn btn-warning"
                                            style="padding: 2px; margin-top: 12px; width: 60%; border: 1px solid #FF9500">
                                            Đã hết
                                            hàng
                                        </td>
                                    @elseif($order->status_order == 'confirm' && $order->vnp_TransactionStatus == '00')
                                        <td class="btn btn-warning"
                                            style="padding: 2px; margin-top: 12px; width: 60%; border: 1px solid #FF9500">
                                            Đã xác
                                            nhận
                                        </td>
                                    @elseif($order->status_order == 'repeat' && $order->vnp_TransactionStatus == '00')
                                        <td class="btn btn-warning"
                                            style="padding: 2px; margin-top: 12px; width: 60%; border: 1px solid #FF9500">
                                            Đã hoàn
                                            hàng
                                        </td>
                                    @elseif( ($order->status_order == 'pending' ) && ( $order->vnp_TransactionStatus == '02' || $order->vnp_TransactionStatus == null ))
                                        <td class="btn btn-warning"
                                            style="padding: 2px; margin-top: 12px; width: 60%; border: 1px solid #FF9500">
                                            Đang chờ
                                            xử lý
                                        </td>
                                    @elseif( ( $order->status_order == 'cancel' || $order->status_order == 'pending' ) && ( $order->vnp_TransactionStatus == '00' || $order->vnp_TransactionStatus == null ))
                                        <td class="btn btn-warning"
                                            style="padding: 2px; margin-top: 12px; width: 60%; border: 1px solid #FF9500">
                                            Đã hủy đơn
                                            hàng
                                        </td>
                                    @elseif($order->status_order == 'shipping' && $order->vnp_TransactionStatus == '00')
                                        <td class="btn btn-warning"
                                            style="padding: 2px; margin-top: 12px; width: 60%; border: 1px solid #FF9500">
                                            Đang giao
                                            hàng
                                        </td>
                                    @elseif($order->status_order == 'success')
                                        <td class="btn btn-warning"
                                            style="padding: 2px; margin-top: 12px; width: 60%; border: 1px solid #FF9500">
                                            Đã giao
                                            hàng
                                        </td>
                                    @elseif($order->status_order == 'finished')
                                        <td class="btn btn-warning"
                                            style="padding: 2px; margin-top: 12px; width: 60%; border: 1px solid #FF9500">
                                            Đã hoàn
                                            thành
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{ $orders->links() }}
    </div>
@endsection