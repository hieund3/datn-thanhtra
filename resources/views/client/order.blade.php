@extends('layouts.account')

@section('section')
    <div class="col-lg-9">
        <div class="main-content">
            <div class="top-content v2 wow fadeInUp">
                <h4 class="title">Đơn hàng của tôi</h4>
            </div>
            <div class="body-content">
                <div class="order-list v2 wow fadeInUp">
                    <div class="body table-responsive">
                        <table class="table order-history-table">
                            <tr>
                                <th>Mã đơn hàng</th>
                                <th>Ngày đặt hàng</th>
                                <th>Tổng tiền</th>
                                <th width="26%">Trạng thái thanh toán</th>
                                <th width="20%">Trạng thái đơn hàng</th>
                                <th>Thao tác</th>
                            </tr>
                            @foreach ($orders as $item)
                                <tr>
                                    <td><a href="{{ route('order.detail',[ 'id' => $item->id])}}" title="">{{$item->vnp_TxnRef}}</a></td>
                                    <td>{{$item->created_at}}</td>
                                    <td><b>{{number_format($item->vnp_Amount)}} VNĐ</b></td>

                                    <!-- Trạng thái thanh toán -->
                                    @if($item->vnp_TransactionStatus == '00')
                                    <td>Thanh toán thành công</td>
                                    @elseif($item->vnp_TransactionStatus == null)
                                    <td>Đã hủy giao dịch</td>
                                    @endif

                                    <!-- Trạng thái đơn hàng -->
                                    @if($item->status_order == 'draft' && $item->vnp_TransactionStatus == '00')
                                    <td style="color: orange">Chờ xác nhận</td>
                                    @elseif($item->status_order == 'pending' && $item->vnp_TransactionStatus == '00')
                                    <td style="color: orange">Đang xử lý</td>
                                    @elseif($item->status_order == 'confirm' && $item->vnp_TransactionStatus == '00')
                                    <td style="color: blue">Đã xác nhận</td>
                                    @elseif( ( $item->status_order == 'cancel' || $item->status_order == 'pending' ) && ( $item->vnp_TransactionStatus == '00' || $item->vnp_TransactionStatus == null ))
                                    <td style="color: red">Đã hủy đơn hàng</td>
                                    @elseif($item->status_order == 'shipping' && $item->vnp_TransactionStatus == '00')
                                    <td style="color: blue">Đang giao hàng</td>
                                    @elseif($item->status_order == 'success' && $item->vnp_TransactionStatus == '00')
                                    <td style="color: green">Đã giao hàng</td>
                                    @elseif($item->status_order == 'finished' && $item->vnp_TransactionStatus == '00')
                                    <td style="color: green">Đã hoàn thành</td>
                                    @endif

                                    {{--Thao tác--}}
                                    <td style="display: flex;justify-content: space-between">
                                        @if($item->status_order === "draft")
                                            <button type="button" class="btn btn-primary getCrypt"
                                                    style="padding: 10px 15px;"
                                                    data-id="{{$item->id}}"
                                                    onclick="crypt({{ $item->id }})"
                                            >
{{--                                                <a href="{{route('order.confirm',['id'=>$item->id])}}"--}}
{{--                                                   style="color: white">--}}
{{--                                                    Lấy mã--}}
{{--                                                </a>--}}
                                                Lấy mã
                                            </button>
                                            <button type="button" class="btn btn-primary ml-3 check-verify" style="padding: 10px 15px;"
                                                    data-dismiss="myModalVerify"
                                                    onclick="verify({{ $item->id }})"
                                            >
                                                    Xác nhận
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                                <div id="myModalVerify" class="modal" style="z-index: 10000">
                                    <!-- Modal content -->
                                    <form action="{{route('order.confirm',['id'=>$item->id])}}" method="post" enctype="multipart/form">
                                    @csrf
                                    <div class="modal-content" style="border-radius: 15px !important;width: 50%;">
                                        <div class="modal-header">
                                            <h1 style="font-weight: bold;font-size: 24px;">Xác nhận đơn hàng</h1>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                                <input type="hidden" name="verify" id="verify">
                                                <p>Thông điệp</p>
                                                <textarea name="message" id="message-verify" cols="30" rows="10" style="width: 100%"></textarea>
                                                <p>Chữ ký</p>
                                                <textarea name="encryptCode" id="textarea-verify" cols="30" rows="10" style="width: 100%"></textarea>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary closeModal" data-dismiss="modal" style="padding: 10px 15px;">
                                                Đóng
                                            </button>
                                            <button type="submit" class="btn btn-primary ml-3" style="padding: 10px 15px;"
                                                    id="confirm"
                                            >
                                                    Xác nhận
                                            </button>
                                        </div>
                                    </div>
                                    </form>
                                </div>

                                <div id="myModal" class="modal" style="z-index: 10000;border-radius: 15px !important;">
                                    <!-- Modal content -->
                                    <div class="modal-content" style="border-radius: 15px !important;width: 50%;">
                                        <div class="modal-header">
                                            <h1 style="font-weight: bold;font-size: 24px;">Mã khóa</h1>
                                            <button type="button" class="close" data-dismiss="myModal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="">
                                                <textarea name="" id="encrypt" cols="30" rows="10" style="width: 100%" readonly></textarea>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary closeModal" data-dismiss="modal" style="padding: 10px 15px;">Đóng</button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </table>
                    </div>
                </div>
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        {{-- <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#"><i class="fal fa-chevron-right"></i></a></li> --}}
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            {{--$(".getCrypt").click(function(e) {--}}
            {{--    e.preventDefault();--}}
            {{--    let id = $('.getCrypt').attr('data-id')--}}

            {{--    $.ajax({--}}
            {{--        url: 'api/get-encrypt/'+id,--}}
            {{--        method: "get",--}}
            {{--        data: {--}}
            {{--            _token: '{{ csrf_token() }}',--}}
            {{--        },--}}
            {{--        success: function(response) {--}}
            {{--            $("#encrypt").val(response)--}}
            {{--            $("#myModal").show()--}}
            {{--        }--}}
            {{--    });--}}
            {{--});--}}

            // $(".check-verify").click(function(e) {
            //     $("#verify").val($('.check-verify').attr('data-oder_id'))
            //     $("#myModalVerify").show()
            // })
            $(".closeModal").click(function(e) {
                $("#myModalVerify").hide()
                $("#myModal").hide()
            })
        });

        function crypt(id){
            $.ajax({
                url: 'api/get-encrypt/'+id,
                method: "get",
                data: {
                    _token: '{{ csrf_token() }}',
                },
                success: function(response) {
                    $("#encrypt").val(response)
                    $("#myModal").show()
                }
            });
        }

        function verify(id){
            $("#verify").val(id)
            $("#textarea-verify").val("")
            $("#message-verify").val("")
            $("#myModalVerify").show()
        }
    </script>

@endsection
