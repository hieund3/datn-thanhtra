@extends('layouts.account')
@section('section')
    {{-- @endsection
    @extends('index')

    @section('content')  --}}

    <div class="col-lg-9">
        <div class="main-content">
            <div class="top-content wow fadeInUp">
                <h4 class="title">Thông tin ví của tôi</h4>
            </div>
            <div class="body-content">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="account-form">
                            @if(!$wallet)
                                <div class="row">
                                    <div class="col-lg-12" style="font-size: large">
                                        Tài khoản của bạn chưa có ví, vui lòng tạo ví.
                                    </div>
                                    <form action="{{route('postWallet')}}" method="post" enctype="multipart/form">
                                        @csrf
                                        <div class="button-update mt-3 mb-3" style="padding-left: 8px;">
                                            <button class="btn" style="width: 9.600000000000001rem;">Tạo ví</button>
                                        </div>
                                    </form>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col-lg-12" style="font-size: large">
                                        Số tài khoản: <b>{{$wallet->account_number ?? null}}</b>
                                    </div>

                                    <div class="col-lg-12 mt-3" style="font-size: large">
                                        Số dư: <b>{{$wallet->amount ?? null}}</b> <span style="color: red">coin</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection